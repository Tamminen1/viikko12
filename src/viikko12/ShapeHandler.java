/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko12;

import java.util.ArrayList;

/**
 *
 * @author Juha
 */
public class ShapeHandler {
    private ArrayList<Point> list = new ArrayList();
    
    public void addShape(int x, int y) {
        list.add(new Point(x,y));
    }
    public int getStartX() {
        return list.get(list.size()-2).getX();
    }
    public int getStartY() {
        return list.get(list.size()-2).getY();
    }
    public int getEndX() {
        return list.get(list.size()-1).getX();
    }
    public int getEndY() {
        return list.get(list.size()-1).getY();
    }           
}
