/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko12;

/**
 *
 * @author Juha
 */
public class Point {
    private final int x;
    private final int y;
    
    public Point(int xx, int yy) {
        x = xx;
        y = yy;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
}
