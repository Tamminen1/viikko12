/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package viikko12;

import static java.lang.Math.abs;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;

/**
 *
 * @author Juha
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private AnchorPane anchor;
    private int x;
    private int y;
    private int temp;
    private int temp2 = 1;
    private ArrayList<Point> list = new ArrayList();
    ShapeHandler sh = new ShapeHandler();
    @FXML
    private Canvas canvas;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void handleButtonAction(MouseEvent event) {
        System.out.println("You clicked me.");
        x = (int)event.getX();
        y = (int)event.getY();
        temp = 0;
        GraphicsContext gc = canvas.getGraphicsContext2D();
        
        for(int i=0; i<list.size(); i++) {
            if(abs(x-list.get(i).getX())<6 && abs(y-list.get(i).getY())<6) {//(Math.sqrt((y-list.get(i).getY())^2+(x-list.get(i).getX())^2))<10
                temp = 1;                                                   //abs(x-list.get(i).getX())<5 && abs(y-list.get(i).getY())<5
                x = list.get(i).getX();
                y = list.get(i).getY();
                break;
            }
        }
        if(temp==0) {
            gc.fillOval(x, y, 10, 10);
            list.add(new Point(x,y));
        }
        else {
            System.out.println("Hei, olen piste!");
            sh.addShape(x, y);
            gc.setLineWidth(4);
            gc.setStroke(Paint.valueOf("red"));
            if(temp2%2==0) {
                if(sh.getStartX()+5!=sh.getEndX()+5 && sh.getStartY()+5!=sh.getEndY()+5) {
                    gc.strokeLine(sh.getStartX()+5, sh.getStartY()+5, sh.getEndX()+5, sh.getEndY()+5);
                }
            }
            temp2++;
        }
    }
}
/*
<?xml version="1.0" encoding="UTF-8"?>

<?import javafx.scene.canvas.*?>
<?import java.lang.*?>
<?import java.util.*?>
<?import javafx.scene.*?>
<?import javafx.scene.control.*?>
<?import javafx.scene.layout.*?>

<AnchorPane id="AnchorPane" fx:id="anchor" onMouseClicked="#handleButtonAction" prefHeight="200" prefWidth="320" xmlns="http://javafx.com/javafx/8" xmlns:fx="http://javafx.com/fxml/1" fx:controller="viikko12.FXMLDocumentController">
   <children>
      <Canvas fx:id="canvas" height="200.0" translateX="-5.0" translateY="-5.0" width="320.0" />
   </children>
</AnchorPane>

*/